require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.ruby_opts = %w[-rtest_helper]
  t.test_files = FileList['test/**/*_test.rb'] - FileList['test/haml/**/*_test.rb'] - FileList['test/hamlit/engine/attributes_test.rb']
end
